{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial gives an example of how to use airt services to train a model and make predictions.\n",
    "\n",
    "We can use the **airt-client** library's following classes for the task at hand:\n",
    "\n",
    "- `Client`  for authenticating and accessing the airt service,\n",
    "\n",
    "- `DataBlob` for encapsulating the data from sources like CSV files, databases, Azure Blob Storage, or AWS S3 bucket, and\n",
    "\n",
    "- `DataSource` for managing datasources and training the models in the airt service.\n",
    "\n",
    "We import them from **airt.client** module as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from airt.client import Client, DataBlob, DataSource"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Authentication \n",
    "\n",
    "To access the airt service, you must first create a developer account. Please fill out the signup form below to get one:\n",
    "\n",
    "- [https://bit.ly/3hbXQLY](https://bit.ly/3hbXQLY)\n",
    "\n",
    "After successful verification, you will receive an email with the username and password for the developer account.\n",
    "\n",
    "Once you have the credentials, use them to get an access token by calling `Client.get_token` method. It is necessary to get an access token; otherwise, you won't be able to access all of the airt service's APIs. You can either pass the username, password, and server address as parameters to the `Client.get_token` method or store them in the environment variables **AIRT_SERVICE_USERNAME**, **AIRT_SERVICE_PASSWORD**, and **AIRT_SERVER_URL**\n",
    "\n",
    "In addition to the regular authentication with credentials, you can also enable multi-factor authentication (MFA) and single sign-on (SSO) for generating tokens.\n",
    "\n",
    "To help protect your account, we recommend that you enable multi-factor authentication (MFA). MFA provides additional security by requiring you to provide unique verification code (OTP) in addition to your regular sign-in credentials when performing critical operations.\n",
    "\n",
    "Your account can be configured for MFA in just two easy steps:\n",
    "\n",
    "1. To begin, you need to enable MFA for your account by calling the `User.enable_mfa` method, which will generate a QR code. You can then \n",
    "scan the QR code with an authenticator app, such as Google Authenticator and follow the on-device instructions to finish the setup in your smartphone.\n",
    "\n",
    "2. Finally, activate MFA for your account by calling `User.activate_mfa` and passing the dynamically generated six-digit verification code from your \n",
    "smartphone's authenticator app.\n",
    "\n",
    "You can also disable MFA for your account at any time by calling the method `User.disable_mfa` method.\n",
    "\n",
    "Single sign-on (SSO) can be enabled for your account in three simple steps:\n",
    "\n",
    "1. Enable the SSO for a provider by calling the `User.enable_sso` method with the SSO provider name and an email address. At the moment, \n",
    "we only support **\"google\"** and **\"github\"** as SSO providers. We intend to support additional SSO providers in future releases.\n",
    "\n",
    "2. Before you can start generating new tokens with SSO, you must first authenticate with the SSO provider. Call the `Client.get_token` with \n",
    "the same SSO provider you have enabled in the step above to generate an SSO authorization URL. Please copy and paste it into your \n",
    "preferred browser and complete the authentication process with the SSO provider.\n",
    "\n",
    "3. After successfully authenticating with the SSO provider, call the `Client.set_sso_token` method to generate a new token and use it automatically \n",
    "in all future interactions with the airt server."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "!!! info\n",
    "\n",
    "\tIn the below example, the username, password, and server address are stored in **AIRT_SERVICE_USERNAME**, **AIRT_SERVICE_PASSWORD**, and **AIRT_SERVER_URL** environment variables.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Authenticate\n",
    "Client.get_token()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. Data Blob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`DataBlob` objects are used to encapsulate data access. Currently, we support:\n",
    "\n",
    "- access for local CSV files,\n",
    "\n",
    "- database access for MySql, ClickHouse, and \n",
    "\n",
    "- files stored cloud storages like AWS S3 bucket and Azure Blob Storage.\n",
    "\n",
    "We intend to support additional databases and storage mediums in future releases.\n",
    "\n",
    "To create a `DataBlob` object, use one of the **DataBlob** class's **from_*** methods. Check out the `DataBlob` class documentation for more information.\n",
    "\n",
    "In this example, the input data is a CSV file stored in an AWS S3 bucket. Before you can use the data to train a model, it must be uploaded to the airt server.\n",
    "To upload data from an AWS S3 bucket to the airt server, use the DataBlob class's `DataBlob.from_s3` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pull the data from an AWS S3 bucket to the airt server\n",
    "data_blob = DataBlob.from_s3(\n",
    "    uri=\"s3://test-airt-service/ecommerce_behavior_csv\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above method will automatically pull the data into the airt server, and all calls to the library are asynchronous and return immediately. To manage completion, all the **from_*** methods of the `DataBlob` class will return a status object indicating the completion status. Alternatively, you can monitor the completion status interactively in a progress bar by calling the `DataBlob.progress_bar` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████| 1/1 [00:35<00:00, 35.40s/it]\n"
     ]
    }
   ],
   "source": [
    "# Display the completion status in a progress bar\n",
    "data_blob.progress_bar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check to ensure that the upload is complete\n",
    "assert data_blob.is_ready()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to preprocess and prepare the data for training. Preprocessing entails creating the index column, sort column, and so on. Currently, CSV and Parquet files can be preprocessed. Please use the `to_datasource` method in the `DataBlob` class for the same. We intend to support additional file formats in the future releases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████| 1/1 [00:40<00:00, 40.46s/it]\n"
     ]
    }
   ],
   "source": [
    "# Preprocess and prepare the data for training.\n",
    "data_source = data_blob.to_datasource(\n",
    "    file_type=\"csv\",\n",
    "    index_column=\"user_id\",\n",
    "    sort_by=\"event_time\"\n",
    ")\n",
    "\n",
    "# Display the data preprocessing progress\n",
    "data_source.progress_bar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When the preprocessing is finished, you can run the following command to display the head of the data to ensure everything is fine.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>event_time</th>\n",
       "      <th>event_type</th>\n",
       "      <th>product_id</th>\n",
       "      <th>category_id</th>\n",
       "      <th>category_code</th>\n",
       "      <th>brand</th>\n",
       "      <th>price</th>\n",
       "      <th>user_session</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>user_id</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10300217</th>\n",
       "      <td>2019-11-06 06:51:52+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>26300219</td>\n",
       "      <td>2053013563424899933</td>\n",
       "      <td>None</td>\n",
       "      <td>sokolov</td>\n",
       "      <td>40.54</td>\n",
       "      <td>d1fdcbf1-bb1f-434b-8f1a-4b77f29a84a0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>253299396</th>\n",
       "      <td>2019-11-05 21:25:44+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>2400724</td>\n",
       "      <td>2053013563743667055</td>\n",
       "      <td>appliances.kitchen.hood</td>\n",
       "      <td>bosch</td>\n",
       "      <td>246.85</td>\n",
       "      <td>b097b84d-cfb8-432c-9ab0-a841bb4d727f</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>253299396</th>\n",
       "      <td>2019-11-05 21:27:43+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>2400724</td>\n",
       "      <td>2053013563743667055</td>\n",
       "      <td>appliances.kitchen.hood</td>\n",
       "      <td>bosch</td>\n",
       "      <td>246.85</td>\n",
       "      <td>b097b84d-cfb8-432c-9ab0-a841bb4d727f</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>272811580</th>\n",
       "      <td>2019-11-05 19:38:48+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>3601406</td>\n",
       "      <td>2053013563810775923</td>\n",
       "      <td>appliances.kitchen.washer</td>\n",
       "      <td>beko</td>\n",
       "      <td>195.60</td>\n",
       "      <td>d18427ab-8f2b-44f7-860d-a26b9510a70b</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>272811580</th>\n",
       "      <td>2019-11-05 19:40:21+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>3601406</td>\n",
       "      <td>2053013563810775923</td>\n",
       "      <td>appliances.kitchen.washer</td>\n",
       "      <td>beko</td>\n",
       "      <td>195.60</td>\n",
       "      <td>d18427ab-8f2b-44f7-860d-a26b9510a70b</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>288929779</th>\n",
       "      <td>2019-11-06 05:39:21+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>15200134</td>\n",
       "      <td>2053013553484398879</td>\n",
       "      <td>None</td>\n",
       "      <td>racer</td>\n",
       "      <td>55.86</td>\n",
       "      <td>fc582087-72f8-428a-b65a-c2f45d74dc27</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>288929779</th>\n",
       "      <td>2019-11-06 05:39:34+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>15200134</td>\n",
       "      <td>2053013553484398879</td>\n",
       "      <td>None</td>\n",
       "      <td>racer</td>\n",
       "      <td>55.86</td>\n",
       "      <td>fc582087-72f8-428a-b65a-c2f45d74dc27</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>310768124</th>\n",
       "      <td>2019-11-05 20:25:52+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>1005106</td>\n",
       "      <td>2053013555631882655</td>\n",
       "      <td>electronics.smartphone</td>\n",
       "      <td>apple</td>\n",
       "      <td>1422.31</td>\n",
       "      <td>79d8406f-4aa3-412c-8605-8be1031e63d6</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>315309190</th>\n",
       "      <td>2019-11-05 23:13:43+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>31501222</td>\n",
       "      <td>2053013558031024687</td>\n",
       "      <td>None</td>\n",
       "      <td>dobrusskijfarforovyjzavod</td>\n",
       "      <td>115.18</td>\n",
       "      <td>e3d5a1a4-f8fd-4ac3-acb7-af6ccd1e3fa9</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>339186405</th>\n",
       "      <td>2019-11-06 07:00:32+00:00</td>\n",
       "      <td>view</td>\n",
       "      <td>1005115</td>\n",
       "      <td>2053013555631882655</td>\n",
       "      <td>electronics.smartphone</td>\n",
       "      <td>apple</td>\n",
       "      <td>915.69</td>\n",
       "      <td>15197c7e-aba0-43b4-9f3a-a815e31ade40</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                          event_time event_type  product_id  \\\n",
       "user_id                                                       \n",
       "10300217   2019-11-06 06:51:52+00:00       view    26300219   \n",
       "253299396  2019-11-05 21:25:44+00:00       view     2400724   \n",
       "253299396  2019-11-05 21:27:43+00:00       view     2400724   \n",
       "272811580  2019-11-05 19:38:48+00:00       view     3601406   \n",
       "272811580  2019-11-05 19:40:21+00:00       view     3601406   \n",
       "288929779  2019-11-06 05:39:21+00:00       view    15200134   \n",
       "288929779  2019-11-06 05:39:34+00:00       view    15200134   \n",
       "310768124  2019-11-05 20:25:52+00:00       view     1005106   \n",
       "315309190  2019-11-05 23:13:43+00:00       view    31501222   \n",
       "339186405  2019-11-06 07:00:32+00:00       view     1005115   \n",
       "\n",
       "                   category_id              category_code  \\\n",
       "user_id                                                     \n",
       "10300217   2053013563424899933                       None   \n",
       "253299396  2053013563743667055    appliances.kitchen.hood   \n",
       "253299396  2053013563743667055    appliances.kitchen.hood   \n",
       "272811580  2053013563810775923  appliances.kitchen.washer   \n",
       "272811580  2053013563810775923  appliances.kitchen.washer   \n",
       "288929779  2053013553484398879                       None   \n",
       "288929779  2053013553484398879                       None   \n",
       "310768124  2053013555631882655     electronics.smartphone   \n",
       "315309190  2053013558031024687                       None   \n",
       "339186405  2053013555631882655     electronics.smartphone   \n",
       "\n",
       "                               brand    price  \\\n",
       "user_id                                         \n",
       "10300217                     sokolov    40.54   \n",
       "253299396                      bosch   246.85   \n",
       "253299396                      bosch   246.85   \n",
       "272811580                       beko   195.60   \n",
       "272811580                       beko   195.60   \n",
       "288929779                      racer    55.86   \n",
       "288929779                      racer    55.86   \n",
       "310768124                      apple  1422.31   \n",
       "315309190  dobrusskijfarforovyjzavod   115.18   \n",
       "339186405                      apple   915.69   \n",
       "\n",
       "                                   user_session  \n",
       "user_id                                          \n",
       "10300217   d1fdcbf1-bb1f-434b-8f1a-4b77f29a84a0  \n",
       "253299396  b097b84d-cfb8-432c-9ab0-a841bb4d727f  \n",
       "253299396  b097b84d-cfb8-432c-9ab0-a841bb4d727f  \n",
       "272811580  d18427ab-8f2b-44f7-860d-a26b9510a70b  \n",
       "272811580  d18427ab-8f2b-44f7-860d-a26b9510a70b  \n",
       "288929779  fc582087-72f8-428a-b65a-c2f45d74dc27  \n",
       "288929779  fc582087-72f8-428a-b65a-c2f45d74dc27  \n",
       "310768124  79d8406f-4aa3-412c-8605-8be1031e63d6  \n",
       "315309190  e3d5a1a4-f8fd-4ac3-acb7-af6ccd1e3fa9  \n",
       "339186405  15197c7e-aba0-43b4-9f3a-a815e31ade40  "
      ]
     },
     "execution_count": null,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Display the first few rows of preprocessed data.\n",
    "data_source.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. Training "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The prediction engine is specialized for predicting which clients are most likely to have a given event in the future.\n",
    "\n",
    "We assume the input data includes the following:\n",
    "\n",
    "- a column identifying a client **client_column** (person, car, business, etc.),\n",
    "\n",
    "- a colum specifying a type of event we will try to predict **target_column** (buy, checkout, click on form submit, etc.), and\n",
    "\n",
    "- a timestamp column specifying the time of an occured event.\n",
    "\n",
    "Each row of data may contain additional columns of the **int**, **category**, **float**, or **datetime** types, which will be used to improve prediction accuracy. E.g. there could be a city associated with each user or type, credit card used for a transaction, smartphone model used to access a mobile app, etc.\n",
    "\n",
    "Finally, we need to know how far ahead we want to make predictions. E.g. if we predict that a client will most likely buy a product in the next minute, there isn't much we can do anyway. We might be more interested in clients who are likely to buy a product tomorrow so that we can send them a special offer or engage them in some other way. That lead time varies greatly depending on the application and can be as short as a few minutes for a web store or as long as several weeks for a banking product such as a loan. In any case, there is a parameter **predict_after** that allows you to specify the time period based on your particular needs.\n",
    "\n",
    "To train a model, pass the configurations for your usecase to the `DataSource.train` method. The train method is asynchronous and may take several hours to complete depending on the size of your dataset. You can check the training status by calling the `Model.is_ready` method or monitor the completion progress interactively by calling the `Model.progress_bar` method.\n",
    "\n",
    "In the following example, we will train a model to predict which users will perform a purchase event **(*purchase)** 3 hours before they actually do it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████| 5/5 [00:00<00:00, 118.02it/s]\n"
     ]
    }
   ],
   "source": [
    "# Train a model\n",
    "from datetime import timedelta\n",
    "\n",
    "model = data_source.train(\n",
    "    client_column=\"user_id\",\n",
    "    target_column=\"event_type\",\n",
    "    target=\"*purchase\",\n",
    "    predict_after=timedelta(hours=3),\n",
    ")\n",
    "\n",
    "# Display model training progress\n",
    "model.progress_bar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check to ensure that the model training is complete\n",
    "assert model.is_ready()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the model training is complete, call the `Model.evaluate` method to display multiple evaluation metrics to evaluate the model's performance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>eval</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>accuracy</th>\n",
       "      <td>0.985</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>recall</th>\n",
       "      <td>0.962</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>precision</th>\n",
       "      <td>0.934</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "            eval\n",
       "accuracy   0.985\n",
       "recall     0.962\n",
       "precision  0.934"
      ]
     },
     "execution_count": null,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Evaluate the model\n",
    "model.evaluate()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. Predictions\n",
    "\n",
    "Finally, you can use the trained model to make predictions by calling the `Model.predict` method. The predict method is asynchronous and may take several hours to complete depending on the size of your dataset. You can check the prediction status by calling the `Prediction.is_ready` method or monitor the completion progress interactively by calling the `Prediction.progress_bar` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████| 3/3 [00:10<00:00,  3.38s/it]\n"
     ]
    }
   ],
   "source": [
    "# Run predictions\n",
    "predictions = model.predict()\n",
    "\n",
    "# Display model prediction progress\n",
    "predictions.progress_bar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check to ensure that the prediction is complete\n",
    "assert predictions.is_ready()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the dataset is small enough, you can download the prediction results as a **Pandas DataFrame** by calling the `Prediction.to_pandas` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Score</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>user_id</th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>520088904</th>\n",
       "      <td>0.979853</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>530496790</th>\n",
       "      <td>0.979157</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>561587266</th>\n",
       "      <td>0.979055</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>518085591</th>\n",
       "      <td>0.978915</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>558856683</th>\n",
       "      <td>0.977960</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>520772685</th>\n",
       "      <td>0.004043</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>514028527</th>\n",
       "      <td>0.003890</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>518574284</th>\n",
       "      <td>0.001346</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>532364121</th>\n",
       "      <td>0.001341</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>532647354</th>\n",
       "      <td>0.001139</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "              Score\n",
       "user_id            \n",
       "520088904  0.979853\n",
       "530496790  0.979157\n",
       "561587266  0.979055\n",
       "518085591  0.978915\n",
       "558856683  0.977960\n",
       "520772685  0.004043\n",
       "514028527  0.003890\n",
       "518574284  0.001346\n",
       "532364121  0.001341\n",
       "532647354  0.001139"
      ]
     },
     "execution_count": null,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Download the prediction results as a pandas DataFrame\n",
    "predictions.to_pandas()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In many cases, it is much better to push the prediction results to destinations such as AWS S3, MySql databases, or even download them to the local machine.\n",
    "\n",
    "Below is an example to push the prediction results to an AWS S3 bucket. For other available options, please check the documentation of the `Prediction` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22-10-28 14:00:48.083 [INFO] __main__: Bucket already created\n"
     ]
    }
   ],
   "source": [
    "# hide\n",
    "# Do not remove \"# hide\" from this cell. Else this cell will appear in documentation\n",
    "\n",
    "import os\n",
    "import boto3\n",
    "from airt.client import User\n",
    "from airt.logger import get_logger, set_level\n",
    "\n",
    "logger = get_logger(__name__)\n",
    "user_details = User.details()\n",
    "DEV_BUCKET_NAME = f'{os.environ[\"STORAGE_BUCKET_PREFIX\"]}-eu-west-1'\n",
    "TEST_OBJECT_NAME = f\"{user_details['uuid']}/tutorial_prediction_results\"\n",
    "TARGET_S3_BUCKET = f\"s3://{DEV_BUCKET_NAME}/{TEST_OBJECT_NAME}\"\n",
    "\n",
    "# Create a new key in the s3 bucket\n",
    "s3_client = boto3.client(\"s3\")\n",
    "\n",
    "try:\n",
    "    s3_client.create_bucket(\n",
    "        Bucket=DEV_BUCKET_NAME,\n",
    "        CreateBucketConfiguration={\"LocationConstraint\": \"eu-west-1\"},\n",
    "    )\n",
    "except s3_client.exceptions.BucketAlreadyOwnedByYou as e:\n",
    "    logger.info(\"Bucket already created\")\n",
    "\n",
    "r = s3_client.put_object(Bucket=DEV_BUCKET_NAME, Key=(TEST_OBJECT_NAME + \"/\"))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████| 1/1 [00:10<00:00, 10.13s/it]\n"
     ]
    }
   ],
   "source": [
    "# Push prediction results to an AWS S3 bucket\n",
    "status = predictions.to_s3(uri=TARGET_S3_BUCKET)\n",
    "\n",
    "# Display prediction push progress\n",
    "status.progress_bar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
