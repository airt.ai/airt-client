# `airt model`

A set of commands for querying the model training, evaluation, and prediction status.

**Usage**:

```console
$ airt model [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion [bash|zsh|fish|powershell|pwsh]`: Install completion for the specified shell.
* `--show-completion [bash|zsh|fish|powershell|pwsh]`: Show completion for the specified shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `details`: Return the details of a model.
* `evaluate`: Return the evaluation metrics of the...
* `ls`: Return the list of models.
* `predict`: Run predictions against the trained model.
* `rm`: Delete a model from the server.

## `airt model details`

Return the details of a model.

**Usage**:

```console
$ airt model details [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Model uuid  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt model evaluate`

Return the evaluation metrics of the trained model.

Currently, this command returns the model's accuracy, precision, and recall. In the future, more performance metrics will be added.

**Usage**:

```console
$ airt model evaluate [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Model uuid.  [required]

**Options**:

* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt model ls`

Return the list of models.

**Usage**:

```console
$ airt model ls [OPTIONS]
```

**Options**:

* `-o, --offset INTEGER`: The number of models to offset at the beginning. If None, then the default value **0** will be used.  [default: 0]
* `-l, --limit INTEGER`: The maximum number of models to return from the server. If None, then the default value **100** will be used.  [default: 100]
* `--disabled`: If set to **True**, then only the deleted models will be returned. Else, the default value **False** will be used to return only the list of active models.
* `--completed`: If set to **True**, then only the models that are successfully downloaded to the server will be returned. Else, the default value **False** will be used to return all the models.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output only uuids of model separated by space
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt model predict`

Run predictions against the trained model.

**Usage**:

```console
$ airt model predict [OPTIONS]
```

**Options**:

* `--data_uuid TEXT`: DataSource uuid.  [required]
* `--uuid TEXT`: Model uuid.  [required]
* `-q, --quiet`: Output the prediction id only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt model rm`

Delete a model from the server.

**Usage**:

```console
$ airt model rm [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Model uuid  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output the deleted Model uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.
