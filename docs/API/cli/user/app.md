# `airt user`

A set of commands for managing users and their authentication in the server.

**Usage**:

```console
$ airt user [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion [bash|zsh|fish|powershell|pwsh]`: Install completion for the specified shell.
* `--show-completion [bash|zsh|fish|powershell|pwsh]`: Show completion for the specified shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `create`: Create a new user in the server.
* `details`: Get user details
* `disable`: Disable a user in the server.
* `enable`: Enable a disabled user in the server.
* `ls`: Return the list of users available in the...
* `mfa`: Commands for enabling and disabling...
* `register-phone-number`: Register and validate a phone number
* `reset-password`: Reset the account password
* `sso`: Commands for enabling and disabling Single...
* `update`: Update existing user details in the server.

## `airt user create`

Create a new user in the server.

**Usage**:

```console
$ airt user create [OPTIONS]
```

**Options**:

* `-un, --username TEXT`: The new user's username. The username must be unique or an exception will be thrown.  [required]
* `-fn, --first_name TEXT`: The new user's first name.  [required]
* `-ln, --last_name TEXT`: The new user's last name.  [required]
* `-e, --email TEXT`: The new user's email. The email must be unique or an exception will be thrown.  [required]
* `-p, --password TEXT`: The new user's password.  [required]
* `-st, --subscription_type TEXT`: User subscription type. Currently, the API supports only the following subscription types **small**, **medium** and **large**.  [required]
* `-su, --super_user`: If set to **True**, then the new user will have super user privilages. If **None**, then the default value **False** will be used to create a non-super user.
* `-ph, --phone_number TEXT`: Phone number to be added to the user account. The phone number should follow the pattern of the country code followed by your phone number. For example, 440123456789, +440123456789, 00440123456789, +44 0123456789,and (+44) 012 345 6789 are all valid formats for registering a UK phone number.
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output user uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt user details`

Get user details

Please do not pass the optional 'user' option unless you are a super user. Only a super user can view details for other users.

**Usage**:

```console
$ airt user details [OPTIONS]
```

**Options**:

* `-u, --user TEXT`: Account user_uuid/username to get details. If not passed, then the currently logged-in details will be returned.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output user uuid only.
* `--help`: Show this message and exit.

## `airt user disable`

Disable a user in the server.

**Usage**:

```console
$ airt user disable [OPTIONS] USERS...
```

**Arguments**:

* `USERS...`: user_uuid/username to disabled.  To disable multiple users, please pass the uuids/names separated by space.  [required]

**Options**:

* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output user uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt user enable`

Enable a disabled user in the server.

**Usage**:

```console
$ airt user enable [OPTIONS] USERS...
```

**Arguments**:

* `USERS...`: user_uuid/username to enable. To enable multiple users, please pass the uuids/names separated by space.  [required]

**Options**:

* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output user uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt user ls`

Return the list of users available in the server.

**Usage**:

```console
$ airt user ls [OPTIONS]
```

**Options**:

* `-o, --offset INTEGER`: The number of users to offset at the beginning. If **None**, then the default value **0** will be used.  [default: 0]
* `-l, --limit INTEGER`: The maximum number of users to return from the server. If None, then the default value 100 will be used.  [default: 100]
* `--disabled`: If set to **True**, then only the deleted users will be returned. Else, the default value **False** will be used to return only the list of active users.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output only user uuids separated by space
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt user mfa`

Commands for enabling and disabling Multi-Factor Authentication (MFA).

**Usage**:

```console
$ airt user mfa [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `disable`: Disable Multi-Factor Authentication (MFA)...
* `enable`: Enable Multi-Factor Authentication (MFA)...

### `airt user mfa disable`

Disable Multi-Factor Authentication (MFA) for the user.

The command switches to interactive mode unless the OTP argument is passed. The interactive mode will prompt you to
choose an OTP option you want to use. Currently, we only support disabling MFA either using a TOTP or SMS OTP.

If you have access to the authenticator application, then you can either enter the dynamically generated six-digit
verification code from the authenticator app (TOTP) or request an OTP via SMS to your registered phone number.

After selecting an option, please follow the on-screen instructions to disable MFA for your account. In case,
you don't have access to the authenticator app and your registered phone number, please contact your administrator.

Note: Please do not pass the user argument unless you are a super user. Only
a super user can disable MFA for other users.

**Usage**:

```console
$ airt user mfa disable [OPTIONS]
```

**Options**:

* `-u, --user TEXT`: Account user_uuid/username to disable MFA. If not passed, then the default value None will be used to disable MFA for the currently logged-in user.
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app or the OTP you have received via SMS.
* `--help`: Show this message and exit.

### `airt user mfa enable`

Enable Multi-Factor Authentication (MFA) for the user.

This is an interactive command and will generate a QR code. You can use an authenticator app, such as Google Authenticator
to scan the code and enter the valid six-digit verification code from the authenticator app in the interactive prompt to
enable and activate MFA for your account.

After three invalid attempts, you have to call this command again to generate a new QR code.

**Usage**:

```console
$ airt user mfa enable [OPTIONS]
```

**Options**:

* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `--help`: Show this message and exit.

## `airt user register-phone-number`

Register and validate a phone number

This is an interactive command, one called it will send an OTP via SMS to the phone number. Please enter the OTP you have received
in the interactive prompt to complete the phone number registration process.

After ten invalid OTP attempts, you have to call this command again to register the phone number.

**Usage**:

```console
$ airt user register-phone-number [OPTIONS]
```

**Options**:

* `-p, --phone-number TEXT`: Phone number to register. The phone number should follow the pattern of the
country code followed by your phone number. For example, **440123456789, +440123456789,
00440123456789, +44 0123456789, and (+44) 012 345 6789** are all valid formats for registering a UK phone number.
If the phone number is not passed in the arguments, then the OTP will be sent to the phone
number that was already registered to the user's account.
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt user reset-password`

Reset the account password

    We currently support two types of OTPs to reset the password for your account and you don't have to be logged in to call this command

    

The command switches to interactive mode unless all arguments are passed. The interactive mode will prompt you for the missing details and ask you to choose a recovery option to reset your password. Currently, we only support resetting the password either using a TOTP or SMS OTP.
    

If you have already activated the MFA for your account, then you can either enter the dynamically generated six-digit verification code from the authenticator app (TOTP) or request an OTP via SMS to your registered phone number.
    

If the MFA is not activated already, then you can only request the OTP via SMS to your registered phone number.
    

After selecting an option, please follow the on-screen instructions to reset your password. In case, you don't have MFA enabled or don't have access to your registered phone number, please contact your administrator.
    

**Usage**:

```console
$ airt user reset-password [OPTIONS]
```

**Options**:

* `-u, --username TEXT`: Account username to reset the password
* `-np, --new-password TEXT`: New password to set for the account
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt user sso`

Commands for enabling and disabling Single sign-on (SSO).

**Usage**:

```console
$ airt user sso [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `disable`: Disable Single sign-on (SSO) for the user.
* `enable`: Enable Single sign-on (SSO) for the user

### `airt user sso disable`

Disable Single sign-on (SSO) for the user.

Please do not pass the user argument unless you are a super user. Only
a super user can disable SSO for other users.

**Usage**:

```console
$ airt user sso disable [OPTIONS] SSO_PROVIDER
```

**Arguments**:

* `SSO_PROVIDER`: Name of the Single sign-on (SSO) identity provider. At present, the API only supports Google and Github as valid SSO identity providers.  [required]

**Options**:

* `-u, --user TEXT`: Account user_uuid/username to disable MFA. If not passed, then the default value None will be used to disable SSO for the currently logged-in user.
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `--help`: Show this message and exit.

### `airt user sso enable`

Enable Single sign-on (SSO) for the user

**Usage**:

```console
$ airt user sso enable [OPTIONS] SSO_PROVIDER
```

**Arguments**:

* `SSO_PROVIDER`: Name of the Single sign-on (SSO) identity provider. At present, the API only supports **Google** and **Github** as valid SSO identity providers.  [required]

**Options**:

* `-e, --email TEXT`: Email id going to be used for SSO authentication.  [required]
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `--help`: Show this message and exit.

## `airt user update`

Update existing user details in the server.

Please do not pass the optional user option unless you are a super user. Only a
super user can update details for other users.

**Usage**:

```console
$ airt user update [OPTIONS]
```

**Options**:

* `--user TEXT`: Account user_uuid/username to update. If not passed, then the default value None will be used to update the currently logged-in user details.
* `-un, --username TEXT`: New username for the user.
* `-fn, --first_name TEXT`: New first name for the user.
* `-ln, --last_name TEXT`: New last name for the user.
* `-e, --email TEXT`: New email for the user.
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if you have activated the MFA for your account.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output user uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.
