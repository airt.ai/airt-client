# `airt pred`

A set of commands for managing and downloading the predictions.

**Usage**:

```console
$ airt pred [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion [bash|zsh|fish|powershell|pwsh]`: Install completion for the specified shell.
* `--show-completion [bash|zsh|fish|powershell|pwsh]`: Show completion for the specified shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `details`: Return the details of a prediction.
* `ls`: Return the list of predictions.
* `rm`: Delete a prediction from the server.
* `to-azure-blob-storage`: Push the prediction results to the target...
* `to-clickhouse`: Push the prediction results to a...
* `to-local`: Download the prediction results to a local...
* `to-mysql`: Push the prediction results to a mysql...
* `to-pandas`: Return the prediction results as a pandas...
* `to-s3`: Push the prediction results to the target...

## `airt pred details`

Return the details of a prediction.

**Usage**:

```console
$ airt pred details [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred ls`

Return the list of predictions.

**Usage**:

```console
$ airt pred ls [OPTIONS]
```

**Options**:

* `-o, --offset INTEGER`: The number of predictions to offset at the beginning. If None, then the default value **0** will be used.  [default: 0]
* `-l, --limit INTEGER`: The maximum number of predictions to return from the server. If None, then the default value **100** will be used.  [default: 100]
* `--disabled`: If set to **True**, then only the deleted predictions will be returned. Else, the default value **False** will be used to return only the list of active predictions.
* `--completed`: If set to **True**, then only the predictions that are successfully downloaded to the server will be returned. Else, the default value **False** will be used to return all the predictions.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output only prediction uuids separated by space.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred rm`

Delete a prediction from the server.

**Usage**:

```console
$ airt pred rm [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `-q, --quiet`: Output the deleted Prediction uuid only.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred to-azure-blob-storage`

Push the prediction results to the target Azure Blob Storage.

**Usage**:

```console
$ airt pred to-azure-blob-storage [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `--uri TEXT`: Target Azure Blob Storage uri.  [required]
* `-c, --credential TEXT`: Credential to access the Azure Blob Storage.  [required]
* `-q, --quiet`: Output status only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred to-clickhouse`

Push the prediction results to a clickhouse database.

If the database requires authentication, pass the username/password as commandline arguments or store it in
the **CLICKHOUSE_USERNAME** and **CLICKHOUSE_PASSWORD** environment variables.

**Usage**:

```console
$ airt pred to-clickhouse [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `--host TEXT`: Remote database host name.  [required]
* `--database TEXT`: Database name.  [required]
* `--table TEXT`: Table name.  [required]
* `--protocol TEXT`: Protocol to use (native/http).  [required]
* `--port INTEGER`: Host port number. If not passed, then the default value **0** will be used.  [default: 0]
* `-u, --username TEXT`: Database username. If not passed, then the value set in the environment variable **CLICKHOUSE_USERNAME** will be used else the default value **root** will be used.
* `-p, --password TEXT`: Database password. If not passed, then the value set in the environment variable **CLICKHOUSE_PASSWORD** will be used else the default value "" will be used.
* `-q, --quiet`: Output status only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred to-local`

Download the prediction results to a local directory.

**Usage**:

```console
$ airt pred to-local [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `--path TEXT`: Local directory path.  [required]
* `-q, --quiet`: Output status only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred to-mysql`

Push the prediction results to a mysql database.

If the database requires authentication, pass the username/password as commandline arguments or store it in
the **AIRT_CLIENT_DB_USERNAME** and **AIRT_CLIENT_DB_PASSWORD** environment variables.

**Usage**:

```console
$ airt pred to-mysql [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `--host TEXT`: Database host name.  [required]
* `--database TEXT`: Database name.  [required]
* `--table TEXT`: Table name.  [required]
* `--port INTEGER`: Host port number. If not passed, then the default value **3306** will be used.  [default: 3306]
* `-u, --username TEXT`: Database username. If not passed, then the value set in the environment variable **AIRT_CLIENT_DB_USERNAME** will be used else the default value **root** will be used.
* `-p, --password TEXT`: Database password. If not passed, then the value set in the environment variable **AIRT_CLIENT_DB_PASSWORD** will be used else the default value "" will be used.
* `-q, --quiet`: Output status only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred to-pandas`

Return the prediction results as a pandas DataFrame.

**Usage**:

```console
$ airt pred to-pandas [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt pred to-s3`

Push the prediction results to the target AWS S3 bucket.

**Usage**:

```console
$ airt pred to-s3 [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Prediction uuid.  [required]

**Options**:

* `--uri TEXT`: The target S3 bucket uri.  [required]
* `--access-key TEXT`: Access key for the target S3 bucket. If **None** (default value), then the value from **AWS_ACCESS_KEY_ID** environment variable is used.
* `--secret-key TEXT`: Secret key for the target S3 bucket. If **None** (default value), then the value from **AWS_SECRET_ACCESS_KEY** environment variable is used.
* `-q, --quiet`: Output status only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.
