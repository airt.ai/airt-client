# `airt api-key`

A set of commands for managing the ApiKeys in the server.

Both the ApiKey and the token can be used for accessing the airt services. However, there is a
slight difference in generating and managing the two.

For generating the ApiKey, you first need to get the developer token. Please refer to **airt token** command documentation to generate one.

After logging in with your developer token, you can create any number of new ApiKeys and can set an
expiration date individually. You can also access other commands available as part of **airt api-key** sub-command to
list, revoke the ApiKeys at any time.

Once the new API key is generated, please set it in the **AIRT_SERVICE_TOKEN** environment variable to start accessing the airt services with it.

**Usage**:

```console
$ airt api-key [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion [bash|zsh|fish|powershell|pwsh]`: Install completion for the specified shell.
* `--show-completion [bash|zsh|fish|powershell|pwsh]`: Show completion for the specified shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `create`: Create a new ApiKey
* `details`: Get the details of an ApiKey.
* `ls`: Get the list of ApiKeys.
* `revoke`: Revoke one or more ApiKeys

## `airt api-key create`

Create a new ApiKey

!!! note

    - The name of the ApiKey must be unique. If not, an exception will be raised while creating a new key with an existing key's name.

    - The expiry for an ApiKey is optional. If not passed, then the default value None will be used to create an ApiKey with no expiry date!

**Usage**:

```console
$ airt api-key create [OPTIONS] NAME
```

**Arguments**:

* `NAME`: The name of the ApiKey.  [required]

**Options**:

* `-e, --expiry INTEGER`: The validity of the API key in number of days. If not passed, then the default value None will be used to create an ApiKey with no expiry date!
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if the MFA is enabled for your account.
* `-q, --quiet`: Output access token only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt api-key details`

Get the details of an ApiKey.

**Usage**:

```console
$ airt api-key details [OPTIONS] APIKEY
```

**Arguments**:

* `APIKEY`: ApiKey uuid/name.  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt api-key ls`

Get the list of ApiKeys.

**Usage**:

```console
$ airt api-key ls [OPTIONS]
```

**Options**:

* `-u, --user TEXT`: user_uuid/username associated with the ApiKey. To get the user account uuid/username, use the `airt user details` command. If the user_uuid/username is not passed, then the currently logged-in user_uuid/username will be used.
* `-o, --offset INTEGER`: The number of ApiKeys to offset at the beginning. If None, then the default value 0 will be used.  [default: 0]
* `-l, --limit INTEGER`: The maximum number of ApiKeys to return from the server. If None, then the default value 100 will be used.  [default: 100]
* `--disabled`: If set to **True**, then the disabled ApiKeys will also be included in the result.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output only ApiKey uuids.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `airt api-key revoke`

Revoke one or more ApiKeys

**Usage**:

```console
$ airt api-key revoke [OPTIONS] KEYS...
```

**Arguments**:

* `KEYS...`: ApiKey uuid/name to revoke. To revoke multiple keys, please pass the uuids/names separated by space.  [required]

**Options**:

* `--user TEXT`: user_uuid/username associated with the ApiKey. To get the user account uuid/username, use the `airt user details` command. If the user_uuid/username is not passed, then the currently logged-in user_uuid/username will be used.
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app. Please pass this optional argument only if the MFA is enabled for your account.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output only the revoked ApiKey uuid(s).
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.
